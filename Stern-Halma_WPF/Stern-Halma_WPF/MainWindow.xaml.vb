﻿Imports System.Xml.Serialization
Imports System.IO

Class MainWindow
#Region "Propiedades"
    Public positions = Nothing
    Public mode As Boolean = False : Dim temporal As New List(Of Integer) : Public capture As Boolean = False
    Public asignGroup As New Dictionary(Of SolidColorBrush, Object)
    Public newGame As New Window(True, False, True, "Stern-Halma", Me)
    Dim reglas = New Reglas(Me) : Public fichas As New List(Of Ficha)
    Public listMovimientos As New Hashtable : Public grids As New List(Of Grid) : Public names As Hashtable
    Public themes As List(Of Theme) : Public mainTheme As Theme : Public urls As New Dictionary(Of Theme, String)
#End Region
#Region "CargarTablero"
    'Metodo para cargar el tablero
    Public Sub TableroLoad()
        Dim arr(12, 16) As Ellipse
        positions = arr
        reglas = New Reglas(Me)
        Dim top As Integer : Dim left As Integer
        For y As Integer = 0 To 16
            If y Mod 2 <> 0 Then
                left += 20
            End If
            For x As Integer = 0 To 12
                positions(x, y) = New Ellipse : positions(x, y).Width = 35 : positions(x, y).Height = 35 : positions(x, y).Fill = Brushes.WhiteSmoke
                Canvas.SetTop(positions(x, y), CDbl(top))
                Canvas.SetLeft(positions(x, y), CDbl(left))
                cvTablero.Children.Add(positions(x, y)) : left += 40
            Next
            top += 35 : left = 0
            If y = 0 Or y = 16 Then
                EliminacionImpar(y, 5)
            ElseIf y = 2 Or y = 14 Then
                EliminacionImpar(y, 4)
            ElseIf y = 1 Or y = 15 Then
                EliminacionPar(y, 4)
            ElseIf y = 3 Or y = 13 Then
                EliminacionPar(y, 3)
            ElseIf y = 5 Or y = 11 Then
                positions(12, y).Fill = Brushes.Transparent
            ElseIf y = 6 Or y = 10 Then
                positions(12, y).Fill = Brushes.Transparent : positions(0, y).Fill = Brushes.Transparent
            ElseIf y = 7 Or y = 9 Then
                positions(12, y).Fill = Brushes.Transparent : positions(11, y).Fill = Brushes.Transparent : positions(0, y).Fill = Brushes.Transparent
            ElseIf y = 8 Then
                positions(0, y).Fill = Brushes.Transparent : positions(1, y).Fill = Brushes.Transparent : positions(12, y).Fill = Brushes.Transparent : positions(11, y).Fill = Brushes.Transparent
            End If
        Next
        If capture = True Then
            positions(6, 8).fill = Brushes.DarkSlateBlue
        End If
        If mode = False Then
            reglas.colorTurno = Brushes.Green
            CargarMarcadores(Brushes.DarkOrange, Me.grUno) : CargarMarcadores(Brushes.Red, Me.grDos) : CargarMarcadores(Brushes.DarkCyan, Me.grTres)
            CargarMarcadores(Brushes.Goldenrod, Me.grCuatro) : CargarMarcadores(Brushes.Purple, Me.grCinco) : CargarMarcadores(Brushes.Green, Me.grSeis)
            Me.PosicionamientoFichasSeis()
        ElseIf mode = True Then
            reglas.colorTurno = Brushes.ForestGreen
            Me.grUno.Visibility = Windows.Visibility.Hidden : Me.grDos.Visibility = Windows.Visibility.Hidden : Me.grSeis.Visibility = Windows.Visibility.Hidden
            CargarMarcadores(Brushes.Purple, Me.grTres) : CargarMarcadores(Brushes.DarkCyan, Me.grCuatro) : CargarMarcadores(Brushes.ForestGreen, Me.grCinco)
            Me.PosicionamientoFichasTres()
        End If
        reglas.ControlTurnos() : reglas.CargarEventos()
    End Sub
    'Metodo que carga los marcadores de movimientos
    Public Sub CargarMarcadores(ByRef color As SolidColorBrush, ByRef grid As Grid)
        Try
            listMovimientos.Add(color, grid) : grids.Add(grid)
            For Each child As Object In grid.Children
                If child.GetType() = GetType(Rectangle) Then
                    Dim rect As Rectangle = CType(child, Rectangle)
                    rect.Fill = color
                    rect.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(mainTheme.MainColor))
                ElseIf child.GetType() = GetType(TextBlock) Then
                    Dim text As TextBlock = CType(child, TextBlock)
                    text.Text = newGame.Names(color).ToString()
                End If
            Next
        Catch es As NullReferenceException : End Try
    End Sub
    'Metodo que posiciona las fichas para mode 6
    Public Sub PosicionamientoFichasSeis()
        temporal.Clear() : temporal.Add(4) : temporal.Add(5) : temporal.Add(6) : temporal.Add(7)
        ColocarFichasExtremas(temporal, 3, Brushes.Red, Brushes.Green)
        AgrupacionesExtremas(temporal, 3, Brushes.Purple, Brushes.DarkCyan)
        temporal.Clear() : temporal.Add(12) : temporal.Add(11) : temporal.Add(10) : temporal.Add(9)
        ColocarFichasExtremas(temporal, 3, Brushes.DarkCyan, Brushes.Purple)
        AgrupacionesExtremas(temporal, 3, Brushes.Green, Brushes.Red)
        temporal.Clear() : temporal.Add(0) : temporal.Add(1) : temporal.Add(2) : temporal.Add(3)
        ColocarFichasPuntas(temporal, Brushes.DarkOrange)
        AgruparPuntas(temporal, Brushes.Goldenrod)
        temporal.Clear() : temporal.Add(16) : temporal.Add(15) : temporal.Add(14) : temporal.Add(13)
        ColocarFichasPuntas(temporal, Brushes.Goldenrod)
        AgruparPuntas(temporal, Brushes.DarkOrange)
    End Sub
    'Metodo para posicionar las fichas en mode 3
    Public Sub PosicionamientoFichasTres()
        temporal.Add(4) : temporal.Add(5) : temporal.Add(6) : temporal.Add(7) : temporal.Add(8)
        ColocarFichasExtremas(temporal, 4, Brushes.Purple, Brushes.ForestGreen)
        temporal.Clear() : temporal.Add(12) : temporal.Add(11) : temporal.Add(10) : temporal.Add(9) : temporal.Add(8)
        AgrupacionesExtremas(temporal, 4, Brushes.ForestGreen, Brushes.Purple)
        temporal.Clear() : temporal.Add(16) : temporal.Add(15) : temporal.Add(14) : temporal.Add(13) : temporal.Add(12)
        ColocarFichasPuntas(temporal, Brushes.DarkCyan)
        temporal.Clear() : temporal.Add(0) : temporal.Add(1) : temporal.Add(2) : temporal.Add(3) : temporal.Add(4)
        AgruparPuntas(temporal, Brushes.DarkCyan)
    End Sub
#End Region
#Region "Colocar Fichas Extremas / Agrupaciones Extremas"
    'Metodo para colocar fichas los extremos izquierdo y derecho
    Public Sub ColocarFichasExtremas(ByRef listay As List(Of Integer), ByRef cantidad As Integer, ByRef color1 As SolidColorBrush, ByRef color2 As SolidColorBrush)
        Dim inter As Integer = 12 : Dim cont As Integer = 1
        For Each y As Integer In listay
            For x As Integer = cantidad To 0 Step -1
                Dim ultimo As Integer = listay.FindLast(Function(value As Integer)
                                                            Return value > 0
                                                        End Function)
                Dim ficha1, ficha2 As New Ficha : fichas.Add(ficha1) : fichas.Add(ficha2)
                ficha1.EllipseC.Fill = color1 : ficha2.EllipseC.Fill = color2
                If capture = True And x = 0 And y = 4 Then
                    ficha1.imgFlag.Visibility = Windows.Visibility.Visible
                    ficha2.imgFlag.Visibility = Windows.Visibility.Visible
                End If
                If capture = True And x = 0 And y = 12 And mode = False Then
                    ficha1.imgFlag.Visibility = Windows.Visibility.Visible
                    ficha2.imgFlag.Visibility = Windows.Visibility.Visible
                End If
                If cont > 2 Then
                    If mode = True And ultimo = y Then
                        Canvas.SetLeft(ficha2, Canvas.GetLeft(positions(inter - x + 2, y)) - 29) : Canvas.SetTop(ficha2, Canvas.GetTop(positions(inter - x + 1, y)) - 32)
                        Canvas.SetLeft(ficha1, Canvas.GetLeft(positions(x + 2, y)) - 29) : Canvas.SetTop(ficha1, Canvas.GetTop(positions(x + 1, y)) - 32)
                    Else
                        Canvas.SetLeft(ficha2, Canvas.GetLeft(positions(inter - x + 1, y)) - 29) : Canvas.SetTop(ficha2, Canvas.GetTop(positions(inter - x + 1, y)) - 32)
                        Canvas.SetLeft(ficha1, Canvas.GetLeft(positions(x + 1, y)) - 29) : Canvas.SetTop(ficha1, Canvas.GetTop(positions(x + 1, y)) - 32)
                    End If
                Else
                    Canvas.SetLeft(ficha1, Canvas.GetLeft(positions(x, y)) - 29) : Canvas.SetTop(ficha1, Canvas.GetTop(positions(x, y)) - 32)
                    Canvas.SetLeft(ficha2, Canvas.GetLeft(positions(inter - x, y)) - 29) : Canvas.SetTop(ficha2, Canvas.GetTop(positions(inter - x, y)) - 32)
                End If
                cvTablero.Children.Add(ficha1) : cvTablero.Children.Add(ficha2)
            Next
            cantidad = cantidad - 1 : inter = inter - 1 : cont = cont + 1
        Next
    End Sub
    'Metodo que hace las agrupaciones de ellipses extremos para ganar
    Public Sub AgrupacionesExtremas(ByRef listay As List(Of Integer), ByRef cantidad As Integer, ByRef colorWin1 As SolidColorBrush, ByRef colorWin2 As SolidColorBrush)
        Dim grupo1 As New List(Of Ellipse) : Dim grupo2 As New List(Of Ellipse)
        Dim inter As Integer = 12 : Dim cont As Integer = 1
        For Each y As Integer In listay
            For x As Integer = cantidad To 0 Step -1
                Dim ultimo As Integer = listay.FindLast(Function(value As Integer)
                                                            Return value > 0
                                                        End Function)
                If cont > 2 Then
                    If mode = True And ultimo = y Then
                        grupo2.Add(positions(inter - x + 2, y)) : grupo1.Add(positions(x + 2, y))
                    Else
                        grupo2.Add(positions(inter - x + 1, y)) : grupo1.Add(positions(x + 1, y))
                    End If
                Else
                    grupo1.Add(positions(x, y)) : grupo2.Add(positions(inter - x, y))
                End If
            Next
            cantidad = cantidad - 1 : inter = inter - 1 : cont = cont + 1
        Next
        asignGroup.Add(colorWin2, grupo2) : asignGroup.Add(colorWin1, grupo1)
    End Sub
#End Region
#Region "Colocar Fichas Puntas / Agrupaciones Puntas"
    'Metodo para coloar las fichas en las puntas
    Public Sub ColocarFichasPuntas(ByRef listay As List(Of Integer), ByRef color As SolidColorBrush)
        Dim ultimo As Integer = listay.FindLast(Function(value As Integer)
                                                    Return value > 0
                                                End Function)
        For Each y As Integer In listay
            For x As Integer = 0 To 12
                Dim ficha As New Ficha : ficha.EllipseC.Fill = color : fichas.Add(ficha)
                If (y = 0 And x = 6 Or y = 16 And x = 6) And mode = False And capture = True Then
                    ficha.imgFlag.Visibility = Windows.Visibility.Visible
                ElseIf (y = 16 And x = 6) And mode = True And capture = True Then
                    ficha.imgFlag.Visibility = Windows.Visibility.Visible
                End If
                If positions(x, y).Fill.Equals(Brushes.Transparent) = False Then
                    If mode = True And y = ultimo Then
                        If x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Then
                            Canvas.SetLeft(ficha, Canvas.GetLeft(positions(x, y)) - 29) : Canvas.SetTop(ficha, Canvas.GetTop(positions(x, y)) - 32)
                            cvTablero.Children.Add(ficha)
                        End If
                    Else
                        Canvas.SetLeft(ficha, Canvas.GetLeft(positions(x, y)) - 29) : Canvas.SetTop(ficha, Canvas.GetTop(positions(x, y)) - 32)
                        cvTablero.Children.Add(ficha)
                    End If
                End If
            Next
        Next
    End Sub

    'Metodo para agrupar los ellipses de las puntas superior e inferior para ganar
    Public Sub AgruparPuntas(ByRef listay As List(Of Integer), ByRef colorGanador As SolidColorBrush)
        Dim grupoTemporal As New List(Of Ellipse)
        Dim ultimo As Integer = listay.FindLast(Function(value As Integer)
                                                    Return value > 0
                                                End Function)
        For Each y As Integer In listay
            For x As Integer = 0 To 12
                If positions(x, y).Fill.Equals(Brushes.Transparent) = False Then
                    If mode = True And y = ultimo Then
                        If x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Then
                            grupoTemporal.Add(positions(x, y))
                        End If
                    Else
                        grupoTemporal.Add(positions(x, y))
                    End If
                End If
            Next
        Next
        asignGroup.Add(colorGanador, grupoTemporal)
    End Sub
#End Region
#Region "Eliminaciones"
    'Metodo que elimina los ellipses sobrantes de la estrella en filas impares
    Public Sub EliminacionImpar(ByRef y As Integer, ByRef limite As Integer)
        For x = 0 To limite
            positions(x, y).Fill = Brushes.Transparent : positions(12 - x, y).Fill = Brushes.Transparent
        Next
    End Sub
    Public Sub EliminacionPar(ByRef y As Integer, ByRef limite As Integer)
        For x = 0 To limite
            positions(x, y).Fill = Brushes.Transparent
        Next
        For x = 0 To (limite + 1)
            positions(12 - x, y).Fill = Brushes.Transparent
        Next
    End Sub
#End Region
#Region "Eventos"
    'Metodo que carga los componentes basicos del programa y el trablero
    Public Sub Window_Initialized(ByVal sender As Object, ByVal e As EventArgs) Handles mainWindow.Initialized
        InitializeComponent()
        Me.CargarTemas()
        newGame.ShowDialog()
        Me.mode = newGame.Mode
        Me.names = newGame.Names
        TableroLoad()
        reglas.CargarEventos()
        If mode = False Then : reglas.colorTurno = Brushes.Green
        Else : reglas.colorTurno = Brushes.ForestGreen : End If
        reglas.ControlTurnos()
    End Sub
    'Metodo para mover la ventana
    Private Sub imgBackground_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles imgBackground.MouseLeftButtonDown, rtPanel.MouseLeftButtonDown
        Me.DragMove()
    End Sub
    'Metodo para cerrar la ventana
    Private Sub imgClose_MouseLeftButtonDown(sender As Object, e As RoutedEventArgs) Handles imgClose.MouseLeftButtonDown
        For Each them As Theme In themes
            them.UrlImage = urls(them)
        Next
        Dim serializer As New XmlSerializer(themes.GetType)
        Dim objStreamWriter As New StreamWriter("Themes.xml")
        serializer.Serialize(objStreamWriter, themes)
        objStreamWriter.Close()
        newGame.Close()
        Me.Close()
    End Sub
    'Metodo para animar el boton de cerrar enter
    Private Sub imgClose_MouseEnter(sender As Object, e As MouseEventArgs) Handles imgClose.MouseEnter
        Me.imgClose.Opacity = 1.0
    End Sub
    'Metodo para animar el boton de cerrar leave
    Public Sub imgClose_MouseLeave(sender As Object, e As MouseEventArgs) Handles imgClose.MouseLeave
        Me.imgClose.Opacity = 0.5
    End Sub
    'Metodo para cambiar de jugadores antes de un movimiento
    Private Sub bt_Click(sender As Object, e As RoutedEventArgs) Handles btTres.Click, btSeis.Click
        If mode = False And sender.Equals(btTres) = True Then
            reglas = Nothing : positions = Nothing : mode = True
            For Each fichax As Ficha In fichas
                cvTablero.Children.Remove(fichax)
            Next
            listMovimientos.Remove(Brushes.Red) : listMovimientos.Remove(Brushes.DarkCyan) : listMovimientos.Remove(Brushes.DarkOrange)
            listMovimientos.Remove(Brushes.Goldenrod) : listMovimientos.Remove(Brushes.Green) : listMovimientos.Remove(Brushes.Purple)
            fichas.Clear() : grids.Clear() : temporal.Clear() : listMovimientos.Clear() : asignGroup.Clear()
            Dim jugadores As New NewGame("Seleccionar jugadores", Me)
            jugadores.ShowDialog()
            newGame.Names.Remove(Brushes.Red) : newGame.Names.Remove(Brushes.DarkCyan) : newGame.Names.Remove(Brushes.DarkOrange)
            newGame.Names.Remove(Brushes.Goldenrod) : newGame.Names.Remove(Brushes.Green) : newGame.Names.Remove(Brushes.Purple)
            newGame.Names.Add(Brushes.Purple, jugadores.Names(0)) : newGame.Names.Add(Brushes.DarkCyan, jugadores.Names(1)) : newGame.Names.Add(Brushes.ForestGreen, jugadores.Names(2))
            TableroLoad()
        ElseIf mode = True And sender.Equals(btSeis) = True Then
            reglas = Nothing : positions = Nothing : mode = False
            For Each fichax As Ficha In fichas
                cvTablero.Children.Remove(fichax)
            Next
            listMovimientos.Remove(Brushes.ForestGreen) : listMovimientos.Remove(Brushes.DarkCyan) : listMovimientos.Remove(Brushes.Purple)
            fichas.Clear() : grids.Clear() : temporal.Clear() : listMovimientos.Clear() : asignGroup.Clear()
            Dim jugadores As New NewGame("Seleccionar jugadores", Me)
            jugadores.ShowDialog()
            newGame.Names.Remove(Brushes.ForestGreen) : newGame.Names.Remove(Brushes.DarkCyan) : newGame.Names.Remove(Brushes.Purple)
            newGame.Names.Add(Brushes.DarkOrange, jugadores.Names(0)) : newGame.Names.Add(Brushes.Red, jugadores.Names(1)) : newGame.Names.Add(Brushes.DarkCyan, jugadores.Names(2))
            newGame.Names.Add(Brushes.Goldenrod, jugadores.Names(3)) : newGame.Names.Add(Brushes.Purple, jugadores.Names(4)) : newGame.Names.Add(Brushes.Green, jugadores.Names(5))
            Me.grUno.Visibility = Windows.Visibility.Visible : Me.grDos.Visibility = Windows.Visibility.Visible : Me.grSeis.Visibility = Windows.Visibility.Visible
            TableroLoad()
        End If
    End Sub
#End Region
#Region "Temas"
    Private Sub btTemas_Click(sender As Object, e As RoutedEventArgs) Handles btTemas.Click
        Dim temas As New Themes(mainTheme.MainColor, Me.themes, Me)
        temas.ShowDialog()
    End Sub
    Private Sub CargarTemas()
        Me.themes = New List(Of Theme)
        Dim serializer As New XmlSerializer(themes.GetType)
        Dim objStreamReader As New StreamReader("Themes.xml")
        themes = serializer.Deserialize(objStreamReader)
        objStreamReader.Close()

        Dim ejecutarrutacompleta = Microsoft.VisualBasic.Right(My.Application.Info.DirectoryPath.Length - 10, My.Application.Info.DirectoryPath.Length)
        Dim RutaDeaplicacion As String = Microsoft.VisualBasic.Left(My.Application.Info.DirectoryPath, ejecutarrutacompleta)
        Dim EjecutarArchivo = RutaDeaplicacion & "\themes\"

        For Each them As Theme In themes
            urls.Add(them, them.UrlImage)
            them.UrlImage = EjecutarArchivo & them.UrlImage
            If them.IsMain = True Then
                Me.mainTheme = them
            End If
        Next

        Me.ThemeSetter(mainTheme)
    End Sub
    Public Sub ThemeSetter(ByRef them As Theme)
        Dim imgBrush = New ImageBrush()
        imgBrush.ImageSource = New BitmapImage(New Uri(them.UrlImage, UriKind.Relative))
        imgBackground.Fill = imgBrush
        imgBackground.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        rtOptions.Fill = New SolidColorBrush(ColorConverter.ConvertFromString(them.GlowColor))
        rtOptions.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        rtBack.Fill = New SolidColorBrush(ColorConverter.ConvertFromString(them.GlowColor))
        rtPanel.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        rtPanel.Fill = New SolidColorBrush(ColorConverter.ConvertFromString(them.GlowColor))
        btTres.Foreground = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        btTres.BorderBrush = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        btSeis.Foreground = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        btSeis.BorderBrush = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
        Me.ColorMarquers(them)
    End Sub
    Public Sub ColorMarquers(ByRef them As Theme)
        For Each gri As Grid In grids
            For Each child As Object In gri.Children
                If child.GetType() = GetType(Rectangle) Then
                    Dim rect As Rectangle = CType(child, Rectangle)
                    rect.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(them.MainColor))
                End If
            Next
        Next
    End Sub
#End Region
End Class
