﻿Imports System.Xml.Serialization

Public Class Theme
    Private _mainColor As String : Private _glowColor As String : Private _UrlImage As String : Private _secondColor
    Private _isMain As Boolean : Private _name As String
    Public Sub New(mainColor As String, glowColor As String, urlImage As String, secondColor As String, isMain As Boolean, _
                   name As String)
        Me._mainColor = mainColor
        Me._glowColor = glowColor
        Me._UrlImage = urlImage
        Me._secondColor = secondColor
        Me._isMain = isMain
        Me._name = name
    End Sub
    Public Sub New()
    End Sub
    <XmlElementAttribute(ElementName:="Nombre")> _
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    <XmlElementAttribute(ElementName:="TemaPrincipal")> _
    Public Property IsMain As String
        Get
            Return _isMain
        End Get
        Set(value As String)
            _isMain = value
        End Set
    End Property
    <XmlElementAttribute(ElementName:="ColorPrincipal")> _
    Public Property MainColor As String
        Get
            Return _mainColor
        End Get
        Set(value As String)
            _mainColor = value
        End Set
    End Property
    <XmlElementAttribute(ElementName:="FondosDesenfocados")> _
    Public Property GlowColor As String
        Get
            Return _glowColor
        End Get
        Set(value As String)
            _glowColor = value
        End Set
    End Property
    <XmlElementAttribute(ElementName:="SegundoColor")> _
    Public Property SecondColor As String
        Get
            Return _secondColor
        End Get
        Set(value As String)
            _secondColor = value
        End Set
    End Property
    <XmlElementAttribute(ElementName:="UrlImagen")> _
    Public Property UrlImage As String
        Get
            Return _UrlImage
        End Get
        Set(value As String)
            _UrlImage = value
        End Set
    End Property
End Class
