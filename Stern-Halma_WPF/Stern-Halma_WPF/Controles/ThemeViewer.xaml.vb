﻿Public Class ThemeViewer
    Private _myTheme As Theme
    Public Property MyTheme As Theme
        Get
            Return _myTheme
        End Get
        Set(value As Theme)
            _myTheme = value
        End Set
    End Property
    Public Sub New(ByRef theme As Theme)
        InitializeComponent()
        Me._myTheme = theme
        Me.InitializeTheme()
    End Sub
    Private Sub InitializeTheme()
        Dim imgBrush = New ImageBrush()
        imgBrush.ImageSource = New BitmapImage(New Uri(_myTheme.UrlImage, UriKind.Relative))
        imgThumb.Fill = imgBrush
        rtStroke.Stroke = New SolidColorBrush(ColorConverter.ConvertFromString(_myTheme.MainColor))
        rtBackName.Fill = New SolidColorBrush(ColorConverter.ConvertFromString(_myTheme.GlowColor))
        tbName.Text = _myTheme.Name
    End Sub

    Private Sub gridView_MouseEnter(sender As Object, e As MouseEventArgs) Handles gridView.MouseEnter, imgThumb.MouseEnter
        Me.rtStroke.Visibility = Windows.Visibility.Visible
    End Sub
    Private Sub gridView_MouseLeave(sender As Object, e As MouseEventArgs) Handles gridView.MouseLeave, imgThumb.MouseLeave
        Me.rtStroke.Visibility = Windows.Visibility.Hidden
    End Sub
End Class
