﻿Public Class NuevoJuego
    Dim texto As String : Dim _mode As Boolean = False : Dim mainWindow As MainWindow
    Dim _names As New Hashtable
    'Propiedad de eleccion de modo de juego
    Public Property Mode As Boolean
        Get
            Return _mode
        End Get
        Set(value As Boolean)
            _mode = value
        End Set
    End Property
    'Propiedad que almacena los nombres de los jugadores
    Public Property Names As Hashtable
        Get
            Return _names
        End Get
        Set(value As Hashtable)
            _names = value
        End Set
    End Property
    'Contructor de la clase NuevoJuego
    Public Sub New(ByVal name As String, ByRef win As MainWindow)
        InitializeComponent()
        mainWindow = win
        lblName.Content = name
        Try
            If mainWindow.mode = False Then
                tbUno.Text = mainWindow.names(Brushes.DarkOrange).ToString : tbDos.Text = mainWindow.names(Brushes.Red).ToString
                tbTres.Text = mainWindow.names(Brushes.DarkCyan).ToString : tbCuatro.Text = mainWindow.names(Brushes.Goldenrod).ToString
                tbCinco.Text = mainWindow.names(Brushes.Purple).ToString : tbSeis.Text = mainWindow.names(Brushes.Green).ToString
            Else
                tbUno.Text = mainWindow.names(Brushes.Purple).ToString : tbDos.Text = mainWindow.names(Brushes.DarkCyan).ToString
                tbTres.Text = mainWindow.names(Brushes.ForestGreen).ToString
            End If
        Catch ex As Exception : End Try
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity down
    Private Sub rtUp_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles rtUp.MouseLeftButtonDown, lblName.MouseLeftButtonDown
        cvContent.Opacity = 0.35
        Me.DragMove()
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity up
    Private Sub selectGame_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        cvContent.Opacity = 1.0
    End Sub
    'Evento para cerrar la ventana
    Private Sub btClose_Click(sender As Object, e As RoutedEventArgs) Handles btClose.Click
        Me.Close()
    End Sub
    'Evento para aceptar los terminos de la ventana
    Private Sub btAceptar_Click(sender As Object, e As RoutedEventArgs) Handles btAceptar.Click
        If tgbSeis.IsChecked = True Or tgbTres.IsChecked = True Then
            If _mode = False Then
                _names.Clear() : _names.Add(Brushes.DarkOrange, tbUno.Text) : _names.Add(Brushes.Red, tbDos.Text) : _names.Add(Brushes.DarkCyan, tbTres.Text)
                MsgBox("1" & tbUno.Text) : MsgBox("2" & tbDos.Text) : MsgBox("3" & tbTres.Text) : MsgBox("4" & tbCuatro.Text) : MsgBox("5" & tbCinco.Text) : MsgBox("6" & tbSeis.Text)
                _names.Add(Brushes.Goldenrod, tbCuatro.Text) : _names.Add(Brushes.Purple, tbCinco.Text) : _names.Add(Brushes.Green, tbSeis.Text)
            Else
                _names.Clear() : _names.Add(Brushes.Purple, tbUno.Text) : _names.Add(Brushes.DarkCyan, tbDos.Text) : _names.Add(Brushes.ForestGreen, tbTres.Text)
            End If
            Me.Close()
        End If
    End Sub
    'Metodo para minimizar la ventana
    Private Sub btMinimize_Click(sender As Object, e As RoutedEventArgs) Handles btMinimize.Click
        Me.WindowState = Windows.WindowState.Minimized
    End Sub
    'Evento de los textbox para eliminar texto
    Private Sub tb_GotKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") = False Then
            texto = sender.Text
            sender.Text = ""
            sender.Foreground = Brushes.LightGray
        End If
    End Sub
    'Evento de los textbox para recuperar texto
    Private Sub tb_LostKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") Then
            sender.Foreground = Brushes.LightGray
            sender.Text = texto
        End If
    End Sub
    'Evento de los togglebutton para activar un modo de juego
    Private Sub toggle_Click(sender As Object, e As RoutedEventArgs) Handles tgbTres.Click, tgbSeis.Click
        If sender.Equals(tgbSeis) Then
            If tgbSeis.IsChecked = True Then
                tgbTres.IsChecked = False : _mode = False
                tbCinco.IsEnabled = True : tbSeis.IsEnabled = True : tbCuatro.IsEnabled = True : tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True
            Else
                tbCinco.IsEnabled = False : tbSeis.IsEnabled = False : tbCuatro.IsEnabled = False : tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
            End If
        ElseIf sender.Equals(tgbTres) = True Then
            If tgbTres.IsChecked = True Then
                tgbSeis.IsChecked = False
                _mode = True
                tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True : tbCinco.IsEnabled = False : tbSeis.IsEnabled = False : tbCuatro.IsEnabled = False
            Else
                tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
            End If
        End If
    End Sub
End Class
