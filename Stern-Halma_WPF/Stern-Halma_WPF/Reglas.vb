﻿Public Class Reglas
    Private main As MainWindow : Private fichaTemporal As Ficha : Dim listColors As New List(Of SolidColorBrush) : Dim colorTemporal As SolidColorBrush
    Dim predicted As New List(Of String) : Dim pastList As List(Of String) : Public colorTurno As SolidColorBrush : Public inicio As Boolean
    Dim verGanar As Boolean : Dim verGanarCapture As Boolean : Dim piezaComida As Object
    'Contructor de la clase Reglas
    Public Sub New(ByRef main As MainWindow)
        Me.main = main
    End Sub
#Region "Movimiento de Fichas"
    'Metodo para marcar los movimientos posibles de las fichas
    Public Sub marcarMovimiento(ByVal sender As Object, ByVal e As EventArgs)
        For y As Integer = 0 To 16
            For x As Integer = 0 To 12
                If (Canvas.GetLeft(main.positions(x, y)) = (Canvas.GetLeft(sender) + 29)) And (Canvas.GetTop(main.positions(x, y)) = Canvas.GetTop(sender) + 32) Then
                    If verGanar = False Or verGanarCapture = False Then
                        If sender.EllipseC.Fill.Equals(colorTurno) = True Then
                            fichaTemporal = sender
                            Me.blanquear() : Me.movimientosNormales(x, y)
                            If main.capture = False Then
                                predicted.Clear() : predicted.Add("" & x & "," & y)
                                If IsNothing(pastList) = False Then : pastList.Clear() : End If
                                Me.PrediccionMovimientos()
                                main.lblControl.Content = "                ok" : main.lblControl.Foreground = Brushes.White
                            Else
                                Me.PrediccionCapture(x, y)
                            End If
                        Else
                            main.lblControl.Content = "Fuera de Turno" : main.lblControl.Foreground = sender.EllipseC.Fill
                        End If
                    End If
                End If
            Next
        Next
    End Sub

    Public Sub GanarCapture(ByRef sender As Ficha)
        If (Canvas.GetLeft(main.positions(6, 8)) = (Canvas.GetLeft(sender) + 29)) And (Canvas.GetTop(main.positions(6, 8)) = (Canvas.GetTop(sender) + 32)) Then
            If sender.imgFlag.Visibility.Equals(Windows.Visibility.Visible) Then
                main.lblControl.Content = "   Ha Ganado!"
                verGanarCapture = True
            End If
        End If
    End Sub
    'METODO PARA VER NUMERO
    Public Function VerNumero(ByRef reference As Ellipse) As Ficha
        For Each fficha In main.fichas
            If (Canvas.GetLeft(reference) = (Canvas.GetLeft(fficha) + 29)) And (Canvas.GetTop(reference) = (Canvas.GetTop(fficha) + 32)) Then
                Return fficha
            End If
        Next
        Return Nothing
    End Function


    'Metodo para marcar los saltos de las fichas
    Public Sub PrediccionMovimientos()
        If predicted.Count() > 0 Then
            Dim predictTemp As New List(Of String)(predicted) : predicted.Clear()
            For Each predict As String In predictTemp
                Dim separar() As String = Split(predict, ",") : Dim x As Integer = Integer.Parse(separar(0)) : Dim y As Integer = Integer.Parse(separar(1))
                Try : If Me.VerificarEncima(main.positions(x + 1, y)) = True And main.positions(x + 2, y).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x + 2, y)) = False Then
                        If pastList.Contains("" & (x + 2) & "," & y) = False Then : predicted.Add("" & (x + 2) & "," & y) : End If
                        main.positions(x + 2, y).Stroke = Brushes.Violet : main.positions(x + 2, y).Fill = Brushes.DarkSlateGray
                    End If : Catch es As Exception : End Try
                Try : If Me.VerificarEncima(main.positions(x - 1, y)) = True And main.positions(x - 2, y).Fill.Equals(Brushes.Transparent) = False _
                          And Me.VerificarEncima(main.positions(x - 2, y)) = False Then
                        If pastList.Contains("" & (x - 2) & "," & y) = False Then : predicted.Add("" & (x - 2) & "," & y) : End If
                        main.positions(x - 2, y).Stroke = Brushes.Violet : main.positions(x - 2, y).Fill = Brushes.DarkSlateGray
                    End If : Catch es2 As Exception : End Try
                If y Mod 2 = 0 Then
                    Try : If Me.VerificarEncima(main.positions(x, y - 1)) = True And main.positions(x + 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                                And Me.VerificarEncima(main.positions(x + 1, y - 2)) = False Then
                            If pastList.Contains("" & (x + 1) & "," & (y - 2)) = False Then : predicted.Add("" & (x + 1) & "," & (y - 2)) : End If
                            main.positions(x + 1, y - 2).Stroke = Brushes.Violet : main.positions(x + 1, y - 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x - 1, y - 1)) = True And main.positions(x - 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                                And Me.VerificarEncima(main.positions(x - 1, y - 2)) = False Then
                            If pastList.Contains("" & (x - 1) & "," & (y - 2)) = False Then : predicted.Add("" & (x - 1) & "," & (y - 2)) : End If
                            main.positions(x - 1, y - 2).Stroke = Brushes.Violet : main.positions(x - 1, y - 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x - 1, y + 1)) = True And main.positions(x - 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                          And Me.VerificarEncima(main.positions(x - 1, y + 2)) = False Then
                            If pastList.Contains("" & (x - 1) & "," & (y + 2)) = False Then : predicted.Add("" & (x - 1) & "," & (y + 2)) : End If
                            main.positions(x - 1, y + 2).Stroke = Brushes.Violet : main.positions(x - 1, y + 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x, y + 1)) = True And main.positions(x + 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                          And Me.VerificarEncima(main.positions(x + 1, y + 2)) = False Then
                            If pastList.Contains("" & (x + 1) & "," & (y + 2)) = False Then : predicted.Add("" & (x + 1) & "," & (y + 2)) : End If
                            main.positions(x + 1, y + 2).Stroke = Brushes.Violet : main.positions(x + 1, y + 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                Else
                    Try : If Me.VerificarEncima(main.positions(x, y - 1)) = True And main.positions(x - 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                                And Me.VerificarEncima(main.positions(x - 1, y - 2)) = False Then
                            If pastList.Contains("" & (x - 1) & "," & (y - 2)) = False Then : predicted.Add("" & (x - 1) & "," & (y - 2)) : End If
                            main.positions(x - 1, y - 2).Stroke = Brushes.Violet : main.positions(x - 1, y - 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x + 1, y - 1)) = True And main.positions(x + 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                                And Me.VerificarEncima(main.positions(x + 1, y - 2)) = False Then
                            If pastList.Contains("" & (x + 1) & "," & (y - 2)) = False Then : predicted.Add("" & (x + 1) & "," & (y - 2)) : End If
                            main.positions(x + 1, y - 2).Stroke = Brushes.Violet : main.positions(x + 1, y - 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x, y + 1)) = True And main.positions(x - 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                          And Me.VerificarEncima(main.positions(x - 1, y + 2)) = False Then
                            If pastList.Contains("" & (x - 1) & "," & (y + 2)) = False Then : predicted.Add("" & (x - 1) & "," & (y + 2)) : End If
                            main.positions(x - 1, y + 2).Stroke = Brushes.Violet : main.positions(x - 1, y + 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                    Try : If Me.VerificarEncima(main.positions(x + 1, y + 1)) = True And main.positions(x + 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                          And Me.VerificarEncima(main.positions(x + 1, y + 2)) = False Then
                            If pastList.Contains("" & (x + 1) & "," & (y + 2)) = False Then : predicted.Add("" & (x + 1) & "," & (y + 2)) : End If
                            main.positions(x + 1, y + 2).Stroke = Brushes.Violet : main.positions(x + 1, y + 2).Fill = Brushes.DarkSlateGray
                        End If : Catch es2 As Exception : End Try
                End If
            Next
            pastList = Nothing : pastList = New List(Of String)(predictTemp)
        End If
        If predicted.Count > 0 Then
            Me.PrediccionMovimientos()
        End If
    End Sub
    Public Sub PrediccionCapture(ByRef x As Integer, ByRef y As Integer)
        Try : If Me.VerificarEncima(main.positions(x + 1, y)) = True And main.positions(x + 2, y).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x + 2, y)) = False Then
                'piezaComida = VerNumero(main.positions(x + 1, y))
                main.positions(x + 2, y).Stroke = Brushes.Violet : main.positions(x + 2, y).Fill = Brushes.DarkSlateGray
            End If : Catch es As Exception : End Try
        Try : If Me.VerificarEncima(main.positions(x - 1, y)) = True And main.positions(x - 2, y).Fill.Equals(Brushes.Transparent) = False _
                  And Me.VerificarEncima(main.positions(x - 2, y)) = False Then
                'piezaComida = VerNumero(main.positions(x - 1, y))
                main.positions(x - 2, y).Stroke = Brushes.Violet : main.positions(x - 2, y).Fill = Brushes.DarkSlateGray
            End If : Catch es2 As Exception : End Try
        If y Mod 2 = 0 Then
            Try : If Me.VerificarEncima(main.positions(x, y - 1)) = True And main.positions(x + 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x + 1, y - 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x, y - 1))
                    main.positions(x + 1, y - 2).Stroke = Brushes.Violet : main.positions(x + 1, y - 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x - 1, y - 1)) = True And main.positions(x - 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x - 1, y - 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x - 1, y - 1))
                    main.positions(x - 1, y - 2).Stroke = Brushes.Violet : main.positions(x - 1, y - 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x - 1, y + 1)) = True And main.positions(x - 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                  And Me.VerificarEncima(main.positions(x - 1, y + 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x - 1, y + 1))
                    main.positions(x - 1, y + 2).Stroke = Brushes.Violet : main.positions(x - 1, y + 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x, y + 1)) = True And main.positions(x + 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                  And Me.VerificarEncima(main.positions(x + 1, y + 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x, y + 1))
                    main.positions(x + 1, y + 2).Stroke = Brushes.Violet : main.positions(x + 1, y + 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
        Else
            Try : If Me.VerificarEncima(main.positions(x, y - 1)) = True And main.positions(x - 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x - 1, y - 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x, y - 1))
                    main.positions(x - 1, y - 2).Stroke = Brushes.Violet : main.positions(x - 1, y - 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x + 1, y - 1)) = True And main.positions(x + 1, y - 2).Fill.Equals(Brushes.Transparent) = False _
                        And Me.VerificarEncima(main.positions(x + 1, y - 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x + 1, y - 1))
                    main.positions(x + 1, y - 2).Stroke = Brushes.Violet : main.positions(x + 1, y - 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x, y + 1)) = True And main.positions(x - 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                  And Me.VerificarEncima(main.positions(x - 1, y + 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x, y + 1))
                    main.positions(x - 1, y + 2).Stroke = Brushes.Violet : main.positions(x - 1, y + 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If Me.VerificarEncima(main.positions(x + 1, y + 1)) = True And main.positions(x + 1, y + 2).Fill.Equals(Brushes.Transparent) = False _
                  And Me.VerificarEncima(main.positions(x + 1, y + 2)) = False Then
                    'piezaComida = VerNumero(main.positions(x + 1, y + 1))
                    main.positions(x + 1, y + 2).Stroke = Brushes.Violet : main.positions(x + 1, y + 2).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
        End If
    End Sub
    'Metodo para marcar los movimientos normales de las fichas
    Public Sub movimientosNormales(ByRef x As Integer, ByRef y As Integer)
        If y Mod 2 = 0 Then
            Try : If main.positions(x + 1, y).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x + 1, y).Stroke = Brushes.Cyan : main.positions(x + 1, y).Fill = Brushes.DarkSlateGray
                End If : Catch es As Exception : End Try
            Try : If main.positions(x, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y - 1).Stroke = Brushes.Cyan : main.positions(x, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x - 1, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x - 1, y - 1).Stroke = Brushes.Cyan : main.positions(x - 1, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x - 1, y).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x - 1, y).Stroke = Brushes.Cyan : main.positions(x - 1, y).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y - 1).Stroke = Brushes.Cyan : main.positions(x, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x, y + 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y + 1).Stroke = Brushes.Cyan : main.positions(x, y + 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x - 1, y + 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x - 1, y + 1).Stroke = Brushes.Cyan : main.positions(x - 1, y + 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
        Else
            Try : If main.positions(x + 1, y).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x + 1, y).Stroke = Brushes.Cyan : main.positions(x + 1, y).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y - 1).Stroke = Brushes.Cyan : main.positions(x, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x + 1, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x + 1, y - 1).Stroke = Brushes.Cyan : main.positions(x + 1, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x - 1, y).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x - 1, y).Stroke = Brushes.Cyan : main.positions(x - 1, y).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x, y - 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y - 1).Stroke = Brushes.Cyan : main.positions(x, y - 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x, y + 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y + 1).Stroke = Brushes.Cyan : main.positions(x, y + 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
            Try : If main.positions(x + 1, y + 1).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x + 1, y + 1).Stroke = Brushes.Cyan : main.positions(x + 1, y + 1).Fill = Brushes.DarkSlateGray
                End If : Catch es2 As Exception : End Try
        End If
    End Sub
    'Metodo para verificar si un ellipse tiene una ficha encima
    Public Function VerificarEncima(ByRef reference As Ellipse) As Boolean
        For Each ficha As Ficha In main.fichas
            If (Canvas.GetLeft(reference) = (Canvas.GetLeft(ficha) + 29)) And (Canvas.GetTop(reference) = (Canvas.GetTop(ficha) + 32)) Then
                Return True
            End If
        Next
        Return False
    End Function
#End Region
#Region "Cambio de posiciones"
    'Metodo de los ellipses para recibir las fichas
    Public Sub recibirFichas(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If sender.Fill.Equals(Brushes.WhiteSmoke) = False And sender.Fill.Equals(Brushes.Transparent) = False _
                And sender.Fill.Equals(Brushes.DarkRed) = False Then
                'Dim xAnterior As Integer = 
                Me.blanquear() : main.cvTablero.Children.Remove(fichaTemporal)
                Canvas.SetLeft(fichaTemporal, Canvas.GetLeft(sender) - 29) : Canvas.SetTop(fichaTemporal, Canvas.GetTop(sender) - 32)
                main.cvTablero.Children.Add(fichaTemporal)
                If main.capture = True Then
                    ' main.cvTablero.Children.Remove(piezaComida)
                End If
                Me.GanarCapture(fichaTemporal)
                fichaTemporal = Nothing : inicio = True
                For Each grid As Grid In main.grids
                    If main.listMovimientos(colorTurno).Equals(grid) = True Then
                        For Each child As Object In grid.Children
                            If child.GetType() = GetType(TextBox) Then
                                Dim text As TextBox = CType(child, TextBox)
                                text.Text = (CInt(text.Text) + 1)
                                Exit For
                            End If
                        Next
                    End If
                Next
                Me.VerificarGanar() : If verGanar = True Then : MsgBox("Ha ganado " & main.newGame.Names(colorTurno).ToString()) : Return : End If
                Me.ControlTurnos()
            ElseIf IsNothing(fichaTemporal) = False And sender.Fill.Equals(Brushes.Transparent) = False Then
                sender.Fill = Brushes.DarkRed : sender.Stroke = Brushes.IndianRed
            End If
        Catch ex As Exception : Console.WriteLine("error") : End Try
    End Sub
    'Metodo que limpia el tablero de las posibles posiciones marcadas
    Public Sub blanquear()
        For y As Integer = 0 To 16
            For x As Integer = 0 To 12
                If main.positions(x, y).Fill.Equals(Brushes.Transparent) = False Then
                    main.positions(x, y).Fill = Brushes.WhiteSmoke : main.positions(x, y).Stroke = Brushes.Transparent
                End If
            Next
        Next
        If main.capture = True Then
            main.positions(6, 8).Fill = Brushes.DarkSlateBlue
        End If
    End Sub
#End Region
#Region "Control de Turnos"
    'Metodo para controlar el flujo de turnos
    Public Sub ControlTurnos()
        If main.mode = False Then
            If colorTurno.Equals(Brushes.Green) = True Then
                colorTurno = Brushes.DarkOrange
            ElseIf colorTurno.Equals(Brushes.DarkOrange) = True Then
                colorTurno = Brushes.Red
            ElseIf colorTurno.Equals(Brushes.Red) = True Then
                colorTurno = Brushes.DarkCyan
            ElseIf colorTurno.Equals(Brushes.DarkCyan) = True Then
                colorTurno = Brushes.Goldenrod
            ElseIf colorTurno.Equals(Brushes.Goldenrod) = True Then
                colorTurno = Brushes.Purple
            ElseIf colorTurno.Equals(Brushes.Purple) = True Then
                colorTurno = Brushes.Green
            End If
        Else
            If colorTurno.Equals(Brushes.ForestGreen) = True Then
                colorTurno = Brushes.Purple
            ElseIf colorTurno.Equals(Brushes.Purple) = True Then
                colorTurno = Brushes.DarkCyan
            ElseIf colorTurno.Equals(Brushes.DarkCyan) = True Then
                colorTurno = Brushes.ForestGreen
            End If
        End If
        Try
            main.tbNombre.Text = main.newGame.Names(colorTurno).ToString() : main.tbNombre.Foreground = colorTurno
            main.elliTurno.Fill = colorTurno
        Catch es As NullReferenceException : End Try
    End Sub
    'Metodo para cargar los eventos de las fichas y los ellipses
    Public Sub CargarEventos()
        For Each elli As Ellipse In main.positions
            If elli.Fill.Equals(Brushes.Transparent) = False Then
                AddHandler elli.MouseLeftButtonDown, AddressOf Me.recibirFichas
            End If
        Next
        For Each _ficha As Ficha In main.fichas
            AddHandler _ficha.MouseLeftButtonDown, AddressOf Me.marcarMovimiento
        Next
    End Sub
#End Region
#Region "Verificar Ganador"
    'Metodo para verificar el ganador de primer nivel que recorre las agrupaciones
    Public Sub VerificarGanar()
        For Each elli As Ellipse In main.asignGroup.Item(colorTurno)
            If VerificarEncimaGanar(elli) = False Then
                Return
            End If
        Next
        verGanar = True
    End Sub
    'Metodo para verificar si las agrupaciones tienen las fichas necesarias para ganar
    Public Function VerificarEncimaGanar(ByRef reference As Ellipse) As Boolean
        For Each ficha As Ficha In main.fichas
            If (Canvas.GetLeft(reference) = (Canvas.GetLeft(ficha) + 29)) And (Canvas.GetTop(reference) = (Canvas.GetTop(ficha) + 32)) Then
                If ficha.EllipseC.Fill.Equals(colorTurno) = True Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function

#End Region
End Class
