﻿Public Class Themes
    Dim converter As New BrushConverter : Dim _mainColorTheme As Color : Dim themes As List(Of Theme)
    Dim mainWindow As MainWindow
    Public Property MainColorTheme
        Get
            Return _mainColorTheme
        End Get
        Set(value)
            _mainColorTheme = value
        End Set
    End Property
    Private Sub topBar_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles rtTopBar.MouseLeftButtonDown, _
        lblNameApplication.MouseLeftButtonDown
        Me.DragMove()
    End Sub
    Public Sub New(ByRef mainColortheme As String, ByRef themes As List(Of Theme), ByRef mainwindow As MainWindow)
        InitializeComponent()
        Me._mainColorTheme = ColorConverter.ConvertFromString(mainColortheme)
        rtMark.Stroke = New SolidColorBrush(_mainColorTheme)
        rtTopBar.Stroke = New SolidColorBrush(_mainColorTheme)
        Me.themes = themes
        Me.mainWindow = mainwindow
        For Each them As Theme In themes
            Dim themeViewer As New ThemeViewer(them)
            If them.IsMain = True Then
                themeViewer.imgCheck.Visibility = Windows.Visibility.Visible
            End If
            AddHandler themeViewer.MouseLeftButtonDown, AddressOf Me.SelectTheme
            spContent.Children.Add(themeViewer)
        Next
    End Sub
    Private Sub SelectTheme(sender As Object, e As RoutedEventArgs)
        Dim send As ThemeViewer
        For Each teamView As Object In spContent.Children
            Dim tv As ThemeViewer = CType(teamView, ThemeViewer)
            If tv.MyTheme.IsMain = True Then
                tv.MyTheme.IsMain = False
                tv.imgCheck.Visibility = Windows.Visibility.Hidden
            End If
            If tv.Equals(sender) Then
                send = tv
                send.MyTheme.IsMain = True
            End If
        Next
        send.imgCheck.Visibility = Windows.Visibility.Visible
        mainWindow.ThemeSetter(send.MyTheme)
    End Sub
    Private Sub cvControles_MouseEnter(sender As Object, e As MouseEventArgs) Handles cvControles.MouseEnter, lblClose.MouseEnter, lblMinimizer.MouseEnter
        btClose.Fill = converter.ConvertFromString("#FFCD0909")
        btMinimizer.Fill = converter.ConvertFromString("#FF246178")
    End Sub
    Private Sub ControlesBT_MouseEnter(sender As Object, e As MouseEventArgs) Handles lblClose.MouseEnter, lblMinimizer.MouseEnter
        sender.Foreground = Brushes.White
    End Sub
    Private Sub ControlWindow(sender As Object, e As MouseEventArgs) Handles cvControles.MouseLeave, btClose.MouseLeave, btMinimizer.MouseLeave
        btClose.Fill = converter.ConvertFromString("#FF4D4949")
        lblClose.Foreground = Brushes.Black
        btMinimizer.Fill = converter.ConvertFromString("#FF4D4949")
        lblMinimizer.Foreground = Brushes.Black
    End Sub
    Private Sub btclose_MouseLeftButtonDown(sender As Object, e As MouseEventArgs) Handles btClose.MouseLeftButtonDown, lblClose.MouseLeftButtonDown
        Me.Close()
    End Sub
    Private Sub btMinimizer_MouseLeftButtonDown(sender As Object, e As MouseEventArgs) Handles btMinimizer.MouseLeftButtonDown, lblMinimizer.MouseLeftButtonDown
        Me.WindowState = Windows.WindowState.Minimized
    End Sub
End Class
