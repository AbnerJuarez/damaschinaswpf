﻿Public Class NewGame
    Dim texto As String : Dim mode As Boolean = False : Dim mainWindow As MainWindow
    Dim _names As New List(Of String) : Public reemplazar As Boolean
    'Propiedad con los nombres obtenidos de la seleccion
    Public Property Names As List(Of String)
        Get
            Return _names
        End Get
        Set(value As List(Of String))
            _names = value
        End Set
    End Property
    'Constructor de la clase Selection
    Public Sub New(ByVal name As String, ByRef win As MainWindow)
        InitializeComponent()
        mainWindow = win
        lblName.Content = name
        If mainWindow.mode = True Then
            tbCinco.IsEnabled = False : tbSeis.IsEnabled = False : tbCuatro.IsEnabled = False : tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
            tbUno.Text = mainWindow.names(Brushes.DarkOrange).ToString : tbDos.Text = mainWindow.names(Brushes.Red).ToString
            tbTres.Text = mainWindow.names(Brushes.DarkCyan).ToString : tbCuatro.Text = mainWindow.names(Brushes.Goldenrod).ToString
            tbCinco.Text = mainWindow.names(Brushes.Purple).ToString : tbSeis.Text = mainWindow.names(Brushes.Green).ToString
        Else
            tbCinco.IsEnabled = True : tbSeis.IsEnabled = True : tbCuatro.IsEnabled = True : tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True
            tbUno.Text = mainWindow.names(Brushes.Purple).ToString : tbDos.Text = mainWindow.names(Brushes.DarkCyan).ToString
            tbTres.Text = mainWindow.names(Brushes.ForestGreen).ToString
            chkUno.IsEnabled = False : chkDos.IsEnabled = False : chkTres.IsEnabled = False : chkCuatro.IsEnabled = False : chkCinco.IsEnabled = False : chkSeis.IsEnabled = False
            tgbJugadores.IsEnabled = False : reemplazar = True
        End If
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity down
    Private Sub rtUp_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles rtUp.MouseLeftButtonDown, lblName.MouseLeftButtonDown
        cvContent.Opacity = 0.35
        Me.DragMove()
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity up
    Private Sub selectGame_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        cvContent.Opacity = 1.0
    End Sub
    'Metodo para aceptar las condiciones de la ventana
    Private Sub btAceptar_Click(sender As Object, e As RoutedEventArgs) Handles btAceptar.Click
        If reemplazar = True Then
            _names.Clear() : _names.Add(tbUno.Text) : _names.Add(tbDos.Text) : _names.Add(tbTres.Text)
            If mainWindow.mode = False Then : _names.Add(tbCuatro.Text) : _names.Add(tbCinco.Text) : _names.Add(tbSeis.Text) : End If
        End If
        If _names.Count = 3 And mainWindow.mode = True Then
            Me.Close()
        ElseIf _names.Count = 6 And mainWindow.mode = False Then
            Me.Close()
        End If
    End Sub
    'Metodo para limpiar texto de los textbox
    Private Sub tb_GotKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") = False Then
            texto = sender.Text
            sender.Text = ""
            sender.Foreground = Brushes.LightGray
        End If
    End Sub
    'Metodo para recuperar texto de los textbox
    Private Sub tb_LostKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") Then
            sender.Foreground = Brushes.LightGray
            sender.Text = texto
        End If
    End Sub
    'Metodo togglebutton para la seleccion de jugadores
    Private Sub toggle_Click(sender As Object, e As RoutedEventArgs) Handles tgbJugadores.Click
        'True si se van a reemplazar con el texto de los textbox, False no
        _names.Clear()
        If tgbJugadores.IsChecked = True Then
            reemplazar = True
            tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True
            chkUno.IsEnabled = False : chkDos.IsEnabled = False : chkTres.IsEnabled = False : chkCuatro.IsEnabled = False : chkCinco.IsEnabled = False : chkSeis.IsEnabled = False
        Else
            reemplazar = False
            chkUno.IsEnabled = True : chkDos.IsEnabled = True : chkTres.IsEnabled = True : chkCuatro.IsEnabled = True : chkCinco.IsEnabled = True : chkSeis.IsEnabled = True
            tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
        End If
    End Sub
    'Metodo para los checkbos para seleccionar los jugadores
    Private Sub checkbox_Checked(sender As Object, e As EventArgs)
        If _names.Count < 3 Then
            If sender.Equals(chkUno) Then
                _names.Add(tbUno.Text)
            ElseIf sender.Equals(chkDos) Then
                _names.Add(tbDos.Text)
            ElseIf sender.Equals(chkTres) Then
                _names.Add(tbTres.Text)
            ElseIf sender.Equals(chkCuatro) Then
                _names.Add(tbCuatro.Text)
            ElseIf sender.Equals(chkCinco) Then
                _names.Add(tbCinco.Text)
            ElseIf sender.Equals(chkSeis) Then
                _names.Add(tbSeis.Text)
            End If
        Else
            sender.isChecked = False
        End If
    End Sub
End Class
