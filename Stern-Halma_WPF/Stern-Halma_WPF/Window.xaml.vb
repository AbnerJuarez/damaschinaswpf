﻿Class Window
    Dim texto As String : Dim _mode As Boolean = False : Dim mainWindow As MainWindow
    Dim _names As New Hashtable
    'Propiedad del modo de juego seleccionado en la ventana
    Public Property Mode As Boolean
        Get
            Return _mode
        End Get
        Set(value As Boolean)
            _mode = value
        End Set
    End Property
    'Propiedad con los nombres de los jugadores
    Public Property Names As Hashtable
        Get
            Return _names
        End Get
        Set(value As Hashtable)
            _names = value
        End Set
    End Property
    'Constructor de la clase Window
    Public Sub New(ByVal minimizer As Boolean, ByVal maximizer As Boolean, ByVal close As Boolean, ByVal name As String, _
                   ByRef win As MainWindow)
        InitializeComponent()
        mainWindow = win
        If minimizer = False Then
            btMinimize.Visibility = Windows.Visibility.Hidden
        End If
        If close = False Then
            btClose.Visibility = Windows.Visibility.Hidden
        End If
        lblName.Content = name
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity down
    Private Sub rtUp_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles rtUp.MouseLeftButtonDown, lblName.MouseLeftButtonDown
        cvContent.Opacity = 0.35
        Me.DragMove()
    End Sub
    'Metodo para animar el movimiento de la ventana, opacity up
    Private Sub selectGame_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        cvContent.Opacity = 1.0
    End Sub
    'Evento para cerrar la clase con un boton
    Private Sub btClose_Click(sender As Object, e As RoutedEventArgs) Handles btClose.Click
        mainWindow.Close()
        Me.Close()
    End Sub
    'Evento para aceptar las condiciones de la ventana
    Private Sub btAceptar_Click(sender As Object, e As RoutedEventArgs) Handles btAceptar.Click
        If tgbSeis.IsChecked = True Or tgbTres.IsChecked = True Then
            If _mode = False Then
                _names.Clear() : _names.Add(Brushes.DarkOrange, tbUno.Text) : _names.Add(Brushes.Red, tbDos.Text) : _names.Add(Brushes.DarkCyan, tbTres.Text)
                _names.Add(Brushes.Goldenrod, tbCuatro.Text) : _names.Add(Brushes.Purple, tbCinco.Text) : _names.Add(Brushes.Green, tbSeis.Text)
            Else
                _names.Clear() : _names.Add(Brushes.Purple, tbUno.Text) : _names.Add(Brushes.DarkCyan, tbDos.Text) : _names.Add(Brushes.ForestGreen, tbTres.Text)
            End If
            Me.Hide()
        End If
    End Sub
    'Metodo para minimizar la ventana
    Private Sub btMinimize_Click(sender As Object, e As RoutedEventArgs) Handles btMinimize.Click
        Me.WindowState = Windows.WindowState.Minimized
    End Sub
    'Metodo para limpiar texto de los textbox
    Private Sub tb_GotKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") = False Then
            texto = sender.Text
            sender.Text = ""
            sender.Foreground = Brushes.LightGray
        End If
    End Sub
    'Metodo para recuperar texto de los textbox
    Private Sub tb_LostKeyboardFocus(ByVal sender As Object, ByVal e As KeyboardFocusChangedEventArgs)
        If sender.Text.Trim().Equals("") Then
            sender.Foreground = Brushes.LightGray
            sender.Text = texto
        End If
    End Sub
    'Metodo para los togglebutton para seleccionar el modo de juego
    Private Sub toggle_Click(sender As Object, e As RoutedEventArgs) Handles tgbTres.Click, tgbSeis.Click
        If sender.Equals(tgbSeis) Then
            If tgbSeis.IsChecked = True Then
                tgbTres.IsChecked = False : _mode = False
                tbCinco.IsEnabled = True : tbSeis.IsEnabled = True : tbCuatro.IsEnabled = True : tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True
            Else
                tbCinco.IsEnabled = False : tbSeis.IsEnabled = False : tbCuatro.IsEnabled = False : tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
            End If
        ElseIf sender.Equals(tgbTres) = True Then
            If tgbTres.IsChecked = True Then
                tgbSeis.IsChecked = False
                _mode = True
                tbUno.IsEnabled = True : tbDos.IsEnabled = True : tbTres.IsEnabled = True : tbCinco.IsEnabled = False : tbSeis.IsEnabled = False : tbCuatro.IsEnabled = False
            Else
                tbUno.IsEnabled = False : tbDos.IsEnabled = False : tbTres.IsEnabled = False
            End If
        End If
    End Sub
    Private Sub Stern_Captura(sender As Object, e As RoutedEventArgs) Handles lblStern.MouseDoubleClick
        mainWindow.capture = True
        lblStern.Foreground = Brushes.DarkRed
    End Sub
End Class
